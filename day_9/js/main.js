#! /usr/bin/env node

// process.arg[2] must be the path to the input file
// process.arg[3] must be the preamble size

fs = require('fs')
xmas = require('./xmas')


function readInput(path) {
    return fs.readFileSync(path, 'UTF-8')
             .split('\n')
             .map(line => line.trim())
             .filter(line => line.length !== 0)
             .map(line => parseInt(line))
}


const numbers = readInput(process.argv[2])
const targetSum = parseInt(process.argv[3])
const xm = new xmas.Xmas(numbers, targetSum)
const invalid = numbers[xmas.findFirstInvalid(xm)]
console.log(invalid)

const interval = xmas.findInterval(xm, invalid)
console.log([Math.min(...interval), Math.max(...interval)])