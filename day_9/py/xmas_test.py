import xmas as xm

numbers = [35,20,15,25,47,40,62,55,65,95,102,117,150,182,127,219,299,277,309,576]


def test_xmas_create():
    xmas = xm.XMas(numbers, 5)
    assert xmas.preamble_start == 0
    assert xmas.preamble_end == 4


def test_xmas_shift():
    xmas = xm.XMas(numbers, 5)
    shifted = xmas.shifted()
    assert shifted.preamble_start == 1
    assert shifted.preamble_end == 5


def test_find_pair():
    l = [5, 7, 1, 123, 52, 44]
    expected = (123, 44)
    actual = xm.find_pair(l, 167)

    assert expected == actual

    actual2 = xm.find_pair(l, 168)
    assert actual2 == ()

    assert xm.find_pair([35, 20, 15, 25, 47], 40) == (15, 25)


def test_find_interval_summing():
    expected = (2, 5)
    actual = xm.find_interval_summing(numbers, 127, numbers[0] + numbers[1], 0, 1)
    assert expected == actual


def test_find_interval_summing_2():
    expected = (6, 11)
    actual = xm.find_interval_summing(numbers, 496, numbers[0] + numbers[1], 0, 1)
    assert expected == actual


def test_find_interval_summing_3():
    expected = (14, 19)
    actual = xm.find_interval_summing(numbers, 1807, numbers[0] + numbers[1], 0, 1)
