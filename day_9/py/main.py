#! /usr/bin/env python3

import xmas
import sys


def main():
    numbers = read_input(sys.argv[1])
    x = xmas.XMas(numbers, 25)
    invalid = x.numbers[xmas.first_invalid_index(x)]
    print(x.numbers[xmas.first_invalid_index(x)])

    interval = xmas.find_interval_summing(numbers, invalid, numbers[0] + numbers[1], 0, 1)
    print((min(interval), max(interval)))


def read_input(path):
    return list(map(lambda entry: int(entry),
        filter(lambda entry: len(entry) != 0,
            map(lambda entry: entry.strip(),
                iter(open(path))))))


main()
