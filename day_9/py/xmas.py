
class XMas:
    def __init__(self, numbers, preamble_size):
        self.numbers = numbers
        self.preamble_start = 0
        self.preamble_end = preamble_size - 1

    def shifted(self):
        x = XMas(self.numbers, self.preamble_end + 1)
        x.preamble_start = self.preamble_start + 1
        x.preamble_end = self.preamble_end + 1
        return x


def first_invalid_index(x):
    """
    Returns the index of the first invalid number or None if
    all the numbers are valid
    """
    index = x.preamble_end + 1
    if index >= len(x.numbers):
        return None
    preamble = x.numbers[x.preamble_start: index]
    if find_pair(preamble, x.numbers[index]) == ():
        return index
    return first_invalid_index(x.shifted())


def find_pair(numbers, target):
    if len(numbers) == 0:
        return ()
    pivot = numbers[0]
    for n in numbers[1:]:
        if pivot + n == target:
            return (pivot, n)
    return find_pair(numbers[1:], target)


def find_interval_summing(numbers, target_sum, current_sum, i, j):
    if current_sum == target_sum:
        return numbers[i: j + 1]
    if j + 1 >= len(numbers):
        return ()
    if current_sum < target_sum:
        return find_interval_summing(numbers, target_sum, current_sum + numbers[j + 1], i, j + 1)
    return find_interval_summing(numbers, target_sum, current_sum - numbers[i], i + 1, j)
