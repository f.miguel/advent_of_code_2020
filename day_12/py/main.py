#! /usr/bin/env python3
import aoc_map as am


def read_input(path):
    return list(map(lambda entry: (entry[0], int(entry[1:])),
                filter(lambda line: len(line) != 0,
                    iter(open(path)))))


def main():
    instructions = read_input('../input.txt')
    actions = {
        'N': am.move_obj_north,
        'S': am.move_obj_south,
        'W': am.move_obj_west,
        'E': am.move_obj_east,
        'F': am.move_obj_forward,
        'L': am.turn_obj_left,
        'R': am.turn_obj_right,
    }
    ship = am.MapObj(0)
    aoc_map = am.AocMap(ship, 0, 0)
    for instruction in instructions:
        actions[instruction[0]](aoc_map, ship, instruction[1])
    print('Part_1: the final ship coordinates are: ' + am.pretty_coordinates(aoc_map, ship))
    print(aoc_map)


if __name__ == '__main__':
    main()
