class AocMap:
    def __init__(self, ship, start_e, start_n):
        self.objects_pos = {}
        self.objects_pos[ship] = (start_e, start_n)

    def __str__(self):
        return f'AocMap {{ objects: {self.objects_pos} }}'

    def inc_obj_n(self, obj, n):
        self.objects_pos[obj] = (self.objects_pos[obj][0], self.objects_pos[obj][1] + n)

    def inc_obj_e(self, obj, e):
        self.objects_pos[obj] = (self.objects_pos[obj][0] + e, self.objects_pos[obj][1])



class MapObj:
    def __init__(self, start_orientation):
        self.orientation = start_orientation

    def turn(self, degrees):
        self.orientation = inc_orientation(self.orientation, degrees)

    def direction(self):
        if self.orientation == 0 or self.orientation == 360:
            return 'E'
        if self.orientation == 90:
            return 'S' 
        if self.orientation == 180:
            return 'W'
        if self.orientation == 270:
            return 'N'
        raise Exception('Unsupported Orientation: ' + str(self.orientation))

    def __str__(self):
        return f'Ship {{ orientation: {self.orientation} }}'


def inc_orientation(orientation, inc):
    if orientation >= 360:
        print()
        print(orientation)
        print(inc)
    if inc >= 0: # right
        print((orientation + inc) % 360)
        return (orientation + inc) % 360
    else: # left
        pos = orientation + inc
        if pos >= 0:
            return pos
        else:
            pot_orientation = orientation + (inc % 360)
            return pot_orientation if pot_orientation >= 0 \
                                   else 360 + pot_orientation


def move_obj_north(aoc_map, obj, units):
    aoc_map.inc_obj_n(obj, units)


def move_obj_south(aoc_map, obj, units):
    aoc_map.inc_obj_n(obj, -units)


def move_obj_east(aoc_map, obj, units):
    aoc_map.inc_obj_e(obj, units)


def move_obj_west(aoc_map, obj, units):
    aoc_map.inc_obj_e(obj, -units)


def move_obj_forward(aoc_map, obj, units):
    key_index = list(aoc_map.objects_pos.keys()).index(obj)
    ship_direction = list(aoc_map.objects_pos.keys())[key_index].direction()
    if ship_direction == 'N':
        move_obj_north(aoc_map, obj, units)
    elif ship_direction == 'S':
        move_obj_south(aoc_map, obj, units)
    elif ship_direction == 'E':
        move_obj_east(aoc_map, obj, units)
    elif ship_direction == 'W':
        move_obj_west(aoc_map, obj, units)
    else:
        raise Exception('Invalid ship direction: ' + ship_direction)


def turn_obj_left(aoc_map, obj, units):
    obj.turn(-units)


def turn_obj_right(aoc_map, obj, units):
    obj.turn(units)


def rotate_arround(aoc_map, obj, center_obj):
    ang = obj


def pretty_coordinates(aoc_map, obj):
    n = aoc_map.objects_pos[obj][1]
    e = aoc_map.objects_pos[obj][0]
    return f"{abs(n)}{'N' if n >= 0 else 'S'}" \
             + f" {abs(e)}{'E' if e > 0 else 'W'}"
