import aoc_map as am


def test_inc_orientation_right():
    actual = am.inc_orientation(0, 0)
    expected = 0
    assert actual == expected

    actual = am.inc_orientation(0, 361)
    expected = 1
    assert actual == expected

    actual = am.inc_orientation(350, 20)
    expected = 10
    assert actual == expected

    actual = am.inc_orientation(360, 1)
    expected = 1
    assert actual == expected


def test_inc_orientation_left():
    actual = am.inc_orientation(0, 0)
    expected = 0
    assert actual == expected

    actual = am.inc_orientation(360, -1)
    expected = 359
    assert actual == expected

    actual = am.inc_orientation(0, -10)
    expected = 350
    assert actual == expected
