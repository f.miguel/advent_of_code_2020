const assert = require('assert')
const layout = require('../layout')

const lay = [
    ['a', 'b', 'c', 'd', 'e'],
    ['f', 'g', 'h', 'i', 'j'],
    ['k', 'l', 'm', 'n', 'o'],
    ['q', 'r', 's', 't', 'u'],
    ['u', 'v', 'w', 'x', 'y'],
    ['z', '1', '2', '3', '4'],
]

const lay2 = [
    ['a', 'b', 'c', 'd', 'e'],
    ['f', 'g', 'h', 'i', 'j'],
    ['k', 'l', 'm', 'n', 'o'],
    ['q', 'r', 's', 't', 'u'],
    ['u', 'v', 'w', 'x', 'y'],
    ['z', '1', '2', '3', '4'],
]

const lay3 = [
    ['a', 'b', 'c', 'd', 'e'],
    ['f', 'g', 'h', 'i', 'j'],
    ['k', 'l', 'm', 'n', 'o'],
    ['q', 'r', 's', 't', 'u'],
    ['u', 'v', 'w', 'x', 'y'],
    ['z', '2', '2', '3', '4'],
]

const lay4 = [
    ['L', '.', '.', '.', '.', '.', '.', '#', '.'],
    ['.', '.', '.', '#', '.', '.', '.', '.', '.'],
    ['.', '#', '.', '.', '.', '.', '.', '.', '.'],
    ['.', '.', '.', '.', '.', '.', '.', '.', '.'],
    ['.', '.', '#', 'L', '.', '.', '.', '.', '#'],
    ['.', '.', '.', '.', '#', '.', '.', '.', '.'],
    ['.', '.', '.', '.', '.', '.', '.', '.', '.'],
    ['#', '.', '.', '.', '.', '.', '.', '.', '.'],
    ['L', '.', '.', '#', '.', '.', '.', '.', '.']
]

const lay5 = [
    ['#', '.', '#', '#', '.', '#', '#', '.', '#', '#'],
    ['#', '#', '#', '#', '#', '#', '#', '.', '#', '#'],
    ['#', '.', '#', '.', '#', '.', '.', '#', '.', '.'],
    ['#', '#', '#', '#', '.', '#', '#', '.', '#', '#'],
    ['#', '.', '#', '#', '.', '#', '#', '.', '#', '#'],
    ['#', '.', '#', '#', '#', '#', '#', '.', '#', '#'],
    ['.', '.', '#', '.', '#', '.', '.', '.', '.', '.'],
    ['#', '#', 'L', '#', '#', '#', '#', '#', '#', '#'],
    ['#', '.', '#', '#', '#', '#', '#', '#', '.', '#'],
    ['#', '.', '#', '#', '#', '#', '#', '.', 'L', '#']
]

describe('Layout', () => {
    describe('#sameElements', () => {
        const arrA = ['C', '2', 't', 'rw']
        const arrB = ['2', 'C', 't', 'rw']
        const arrC = ['0', 'C', 't', 'rw']
        it('should return true for equal arrays', () => {
            assert.equal(layout.sameElements(arrA, arrB), true)
        })
        it('should return false for different arrays', () => {
            assert.equal(layout.sameElements(arrA, arrC), false)

        })
    })
    describe('#getAdjacents', () => {
        it('top left corner', () => {
            assert.equal(layout.sameElements(layout.getAdjacents(lay, 0, 0), ['b', 'f', 'g']), true)
        })
        it('top right corner', () => {
            assert.equal(layout.sameElements(layout.getAdjacents(lay, 0, 4), ['i', 'j', 'd']), true)
        })
        it('bot left corner', () => {
            assert.equal(layout.sameElements(layout.getAdjacents(lay, 5, 0), ['u', 'v', '1']), true)
        })
        it('bot right corner', () => {
            assert.equal(layout.sameElements(layout.getAdjacents(lay, 5, 4), ['x', 'y', '3']), true)
        })
        it('not in corner', () => {
            assert.equal(layout.sameElements(layout.getAdjacents(lay, 2, 2), ['g', 'h', 'i', 'l', 'r', 's', 't', 'n']), true)
        })
    })
    describe('#equal', () => {
        it('should return true for equal layouts', () => {
            assert.equal(layout.equal(lay, lay2), true)
        })
        it('should return false for different layouts', () => {
            assert.equal(layout.equal(lay, lay3), false)
        })
        it('should true when both layouts are empty', () => {
            assert.equal(layout.equal([[]], [[]]), true)
        })
        it('should false when one of the layouts is empty', () => {
            assert.equal(layout.equal(lay, [[]]), false)
        })
    })
    describe('#getNearests', () => {
        it('not in corner', () => {
            const actual = layout.getNearests(lay4, 4, 3)
            const expected = ['#', '#', '#', '#', '#', '#', '#', '#']
            assert.deepStrictEqual(actual, expected)
        })
        it('top left corner', () => {
            const actual = layout.getNearests(lay4, 0, 0)
            const expected = ['#', '#']
            assert.deepStrictEqual(actual, expected)
        })
        it('bot right corner', () => {
            const actual = layout.getNearests(lay4, 8, 0)
            const expected = ['#', '#']
            assert.deepStrictEqual(actual, expected)
        })
        it('top right corner', () => {
            const actual = layout.getNearests(lay5, 0, 9)
            const expected = ['#', '#', '#']
            assert.deepStrictEqual(actual, expected)
        })
        it('another not in corner', () => {
            const actual = layout.getNearests(lay5, 6, 2)
            const expected = ['#', '#', '#', '#', '#', '#', 'L']
            assert.equal(layout.sameElements(actual, expected), true)
        })
    })
})
