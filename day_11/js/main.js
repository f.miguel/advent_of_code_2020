#! /usr/bin/env node

const fs = require('fs');
const lay = require('./layout');


function readInput(path) {
    return fs.readFileSync(path, 'UTF-8')
            .split('\n') // lines
            .map(line => line.trim())
            .filter(line => line.length !== 0)
            .map(line => line.split(''));
}

console.log(`Part_1: ${lay.countOccupied(lay.stabilize(readInput('../input.txt'), 4, lay.getAdjacents))}`
            + ' occupied seats after stabilization!');


console.log(`Part_2: ${lay.countOccupied(lay.stabilize(readInput('../input.txt'), 5, lay.getNearests))}`
            + ' occupied seats after stabilization!');
