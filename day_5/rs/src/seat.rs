use std::{ops::Range};
use std::fmt::{self, write};

#[derive(Debug, Hash, Eq, PartialEq)]
pub struct Seat {
    pub id: usize,
    row: usize,
    col: usize
}

impl Seat {
    pub fn from(bin_code: &str) -> Seat {
        let pos = Self::find_seat(bin_code, 128, 8);
        Seat {
            id: pos.0 * 8 + pos.1,
            row: pos.0,
            col: pos.1
        }
    }

    pub fn from_pos(row: usize, col: usize) -> Seat {
        Seat {
            id: row * 8 + col,
            row,
            col
        }
    }

    fn find_seat(bin_code: &str, total_rows: usize, total_columns: usize) -> (usize, usize) {
        let row_code: String = bin_code.chars().take(7).collect();
        let column_code: String = bin_code.chars().skip(7).map(|c| if c == 'L' {'F'} else {'B'}).collect();

        let row = Self::part(row_code.as_str(), 0, total_rows).start;
        let column = Self::part(column_code.as_str(), 0, total_columns).start;
        return (row, column)
    }

    fn part(row_code: &str, start: usize, end: usize ) -> Range<usize>{
        if row_code.len() == 0 {
            return start..end;
        }
        let char: char = row_code.chars().nth(0).unwrap();
        return if char == 'F' {
            Self::part(&row_code[1..], start, end - (end -start) / 2)
        } else {
            Self::part(&row_code[1..], start + (end - start) / 2, end)
        }
    }
}

impl fmt::Display for Seat {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Seat (id: {}, row: {}, col: {})", self.id, self.row, &self.col)
    }
}