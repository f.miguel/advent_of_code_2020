#! /bin/env python3

from seat import from_code
from seat import Seat
import sys
from functools import reduce


def main():
    entries = list(filter(lambda entry: len(entry) != 0,
                          map(lambda entry: entry.strip(), iter(open(sys.argv[1])))))
    found_seats = {}
    for entry in entries:
        s = from_code(entry)
        found_seats[s.id] = s

    largest_id = reduce(lambda x, y: x if x > y else y,
                        map(lambda s: s.id, found_seats.values()))
    print(f'The largest seat id is {largest_id}')
    print('The following seats are missing:')

    missing = find_missing(found_seats)
    for seat in missing:
        print(seat)


# found must be a dictionary
def find_missing(found):
    all_seats = []
    missing = []
    for i in range(0, 128):
        for j in range(0, 8):
            all_seats.append(Seat(i, j))

    for seat in all_seats:
        if seat.id not in found:
            missing.append(seat)
    return missing


main()
