from more_itertools import take


class Seat:
    def __init__(self, row: int,  col: int):
        self.id = row * 8 + col
        self.row = row
        self.col = col

    def __str__(self):
        return f'Seat(id: {self.id}, row: {self.row}, col: {self.col})'


def from_code(code: str):
    row = part(code[0:8], 0, 128)
    col_code = ''.join(map(lambda x: 'F' if x == 'L' else 'B', iter(code[7:])))
    col = part(col_code, 0, 8)
    return Seat(row, col)


# start is inclusive, end is exclusive
def part(code: str, start, end):
    if len(code) == 0:
        return start

    if code[0] == 'F':
        return part(code[1:], start, end - (end - start) // 2)
    else:
        return part(code[1:], start + (end - start) // 2, end)
