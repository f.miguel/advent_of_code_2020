use std::collections::VecDeque;

pub struct IMap {
    patt: Vec<Vec<char>>
}

impl IMap {

    pub fn new(patt: Vec<Vec<char>>) -> IMap {
        IMap {
            patt
        }
    }

    pub fn traverse_count(&self, start: (usize, usize), slope:(usize, usize), sym: char) -> usize {
        self.path(start, slope)
            .iter()
            .filter(|&s| *s == sym)
            .count()
    }

    /// Finds the path to the botton starting from `pos` and following the given `slope`.
    fn path(&self, start:(usize, usize), slope:(usize, usize)) -> VecDeque<char> {
        if start.1 >= self.patt.len() {
            return VecDeque::new();
        }

        // check if next x index is out of bounds
        let next_x = start.0 + slope.0;
        let patt_width = self.patt[0].len(); // assumes patt.len > 0
        let slope_x = match patt_width <= next_x {
                        true => next_x - patt_width,
                        false => next_x
        };

        let mut v = self.path((slope_x, start.1 + slope.1), slope);
        v.insert(0, self.patt[start.1][start.0]);
        return v
    }

    pub fn patt(&self) -> &Vec<Vec<char>> {
        &self.patt
    }
}

#[cfg(test)]
mod tests {

    use crate::island_map::IMap;
    use std::collections::VecDeque;

    fn test_map() -> IMap {
        IMap::new(
                vec![
                    vec!['#', '.', '.', '#', '.', '#', '#', '.'],
                    vec!['#', '.', '.', '#', '.', '#', '#', '.'],
                    vec!['#', '.', '.', '#', '.', '#', '#', '.'],
                    vec!['#', '.', '.', '#', '.', '#', '#', '.'],
                    vec!['#', '.', '.', '#', '.', '#', '#', '.'],
                    vec!['#', '.', '.', '#', '.', '#', '#', '.'],

                ]
            )
    }

    #[test]
    fn test_path() {
        
        let expected = VecDeque::from(vec!('#', '.', '.', '#', '.', '#'));
        assert_eq!(expected, test_map().path((0,0), (1, 1)));

        // with transbordation
        let expected = VecDeque::from(vec!('.', '#', '#', '.', '.', '#'));
        assert_eq!(expected, test_map().path((4, 0), (2, 1)));
    }

    #[test]
    fn test_traverse_count() {
        assert_eq!(3, test_map().traverse_count((0,0), (1,1), '#'))
    }
}