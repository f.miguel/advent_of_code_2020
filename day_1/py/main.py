import sys
import functools as ft


def find_pair(list, target):
    if len(list) == 1:
        return ()

    pivot = list[0]
    for n in list[1:]:
        if pivot + n == target:
            return pivot, n

    return find_pair(list[1:], target)


def find_triple(list, target):
    if len(list) < 3:
        return ()

    pivot = list[0]
    pair = find_pair(list[1:], target - pivot)
    if pair != ():
        return pivot, pair[0], pair[1]

    return find_triple(list[1:], target)


def main():
    entries = map(lambda x: int(x),
                  filter(lambda line: len(line.strip()) != 0 and line.strip()
                         .isdigit(),
                         iter(open(sys.argv[1]))))

    triple = find_triple(list(entries), int(sys.argv[2]))
    print(triple)
    print(ft.reduce(lambda x, y: x * y, triple))


main()
