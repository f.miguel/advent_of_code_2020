package advent_of_code.day_four

import java.io.File

fun main(args: Array<String>) {
    val passports = parsePassports(readEntries(File("day_four/input.txt").readLines()))
    val validPassports = passports.filter(::isValid)
    println("Found ${validPassports.count()} valid passports")

}

fun readEntries(lines: List<String>): List<String> {
    return if (lines.isEmpty()) {
        listOf()
    } else {
        listOf(concatStrings(lines.takeWhile { it.isNotBlank() }))
            .plus(readEntries(lines.dropWhile { it.isNotBlank() }
                                   .drop(1))) // drop blank line too
    }
}

fun parsePassports(s: List<String>) = s.map(::parsePassportEntry)

fun parsePassportEntry(s: String): Passport {

    var birthYear = ""
    var issueYear = ""
    var expirationYear = ""
    var height = ""
    var hairColour = ""
    var eyeColour = ""
    var passportId = ""
    var countryId = ""

    s.split(' ').forEach {
        val part = it.split(':')
        val key = part[0]
        val value = part[1]

        when (key) {
            "byr" -> birthYear = value
            "iyr" -> issueYear = value
            "eyr" -> expirationYear = value
            "hgt" -> height = value
            "hcl" -> hairColour = value
            "ecl" -> eyeColour = value
            "pid" -> passportId = value
            "cid" -> countryId = value
        }
    }
    return Passport(
        passportId,
        birthYear,
        issueYear,
        expirationYear,
        height,
        hairColour,
        eyeColour,
        countryId
    )
}

fun concatStrings(strings: List<String>) = strings.reduce { acc, s -> acc.plus(' ').plus(s) }
