package advent_of_code.day_two

interface PasswordPolicy {

    fun checkCompliance(str: String) : Boolean
}