use std::collections::HashSet;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use internment::Intern;

#[derive(Debug)]
struct Bag<'a> {
    color_code: String,
    can_contain: HashMap<&'a Bag<'a>, u32>
}

impl<'a> Bag<'a> {
    fn new(color_code: String) -> Bag<'a> {
        Bag {
            color_code,
            can_contain: HashMap::new()
        }
    }

    fn add_containable_bag(&mut self, bag: &'a Bag, n: u32) {
        self.can_contain.insert(bag, n);
    }

    fn may_contain(&self, bag: &Bag) -> bool{
        self.can_contain.contains_key(bag)
    }
}

impl PartialEq for Bag<'_> {
    fn eq(&self, other: &Bag) -> bool {
        return self.color_code == other.color_code;
    }
}

impl Eq for Bag<'_> {}

impl Hash for Bag<'_> {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.color_code.hash(hasher)
    }
}


struct BagFactory {}

impl BagFactory {

    fn new() -> BagFactory {
        BagFactory {}
    }

    fn bag_of(&mut self, color_code: String) -> &Bag {
        let bag = Bag::new(color_code);
        return Intern::new(bag).as_ref();
    }
}

#[cfg(test)]
mod tests {

    use crate::bag::BagFactory;

    #[test]
    fn test_factory_internment() {
        let mut factory = BagFactory::new();
        let b1 = factory.bag_of(String::from("dark blue"));
        let b2 = factory.bag_of(String::from("dark blue"));
        assert_eq!(b1, b2)
    }
}