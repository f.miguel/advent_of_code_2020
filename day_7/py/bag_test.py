from bag import Bag
from bag import bag_of


def test_contains():
    b1 = bag_of('muted yellow')
    b2 = bag_of('muted green')
    b3 = bag_of('muted blue')
    b4 = bag_of('muted ruge')
    b1.add_containable_bag(b2, 2)
    b1.add_containable_bag(b3, 1)
    assert b1.may_contain(b2) and b1.may_contain(b2)
    assert not b1.may_contain(b4)
    assert not b1.may_contain(b1)
    assert not b2.may_contain(b1)


def test_equals_and_hash():
    bag1 = Bag('muted yellow')
    bag2 = Bag('muted yellow')
    bag3 = Bag('muted blue')

    assert bag1 == bag1
    assert bag1 == bag2
    assert bag1 != bag3
    assert bag1 is not None


def test_factory_internment():
    b1 = bag_of('mute yellow')
    b2 = bag_of('mute yellow')
    b3 = bag_of('mute green')

    assert b1 is b2
    assert b1 is not b3
