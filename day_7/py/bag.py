from functools import reduce
from typing import List
from typing import Tuple
from typing import Dict


class Bag:
    def __init__(self, color_code: str):
        self.color_code: str = color_code
        self.can_contain: List[Tuple[Bag, int]] = []

    def add_containable_bag(self, bag, n):
        self.can_contain.append((bag, n))

    def sub(self):
        return self.can_contain

    def may_contain(self, bag):
        for b in self.can_contain:
            if b[0] == bag:
                return True
        return False

    def __str__(self):
        return f'{self.color_code} bag contain ' + self.containable_bags_str()

    def containable_bags_str(self) -> str:
        if len(self.can_contain) == 0:
            return 'no other bags'
        result = reduce(lambda acc, entry: acc + ' ' + entry,
                        map(lambda entry: str(entry[1]) + ' ' + entry[0].color_code
                            + ' bags,' if entry[1] > 0 else ' bag,',
                            self.can_contain))

        # replace the last ',' with '.'
        return result[0: len(result) - 1] + '.'

    def __eq__(self, other):
        if self is other:
            return True
        if other is None or not isinstance(other, Bag):
            return False
        return self.color_code == other.color_code

    def __hash__(self):
        return self.color_code.__hash__()


# interned bags data structure
interned: Dict[Bag, Bag] = {}


def bag_of(color_code: str):
    """Returns a interned Bag instance instantiated with the given args"""
    return intern(Bag(color_code))


def intern(bag: Bag):
    """Interns the given bag"""
    if bag in interned:
        return interned[bag]
    interned[bag] = bag
    return bag
