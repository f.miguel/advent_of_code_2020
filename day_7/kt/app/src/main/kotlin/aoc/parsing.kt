package aoc

fun parseEntry(entry: String): Bag{
    val split = entry.dropLast(1)
                .split("bags contain")
                .map(String::trim);
    val bag = Bag.of(split[0])
    if (split[1] != "no other bags") {
        val subBags: List<Pair<Bag, Int>> = split[1]
            .split(", ")
            .map(String::trim)
            .map(::parseSubBagEntry)
        subBags.forEach {subBag -> bag.addContainableBag(subBag.first, subBag.second)}
    }
    return bag
}

private fun parseSubBagEntry(entry: String): Pair<Bag, Int> {
    val n = entry.substring(0, 1).toInt()
    val bag = Bag.of(entry.substring(2, entry.length - 5))
    return Pair(bag, n)
}