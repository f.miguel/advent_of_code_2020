package aoc

class Bag private constructor(val colorCode: String) {

    companion object Factory {

        private val interned: MutableMap<Bag, Bag> = HashMap()

        fun of(colorCode: String): Bag = intern(Bag(colorCode))

        private fun intern(bag: Bag): Bag {
            if (interned.containsKey(bag)) {
                return interned[bag]!!
            }
            interned[bag] = bag
            return bag
        }
    }

    private val canContain: MutableMap<Bag, Int> = HashMap<Bag, Int>()

    fun contains(bag: Bag) = canContain.containsKey(bag)

    fun addContainableBag(bag: Bag, n: Int) {
        canContain[bag] = n
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is Bag) return false

        return this.colorCode == other.colorCode
    }

    override fun hashCode() = colorCode.hashCode()

    override fun toString() =
        "Bag: " + colorCode +
                canContainStr() + "\n"

    private fun canContainStr() =
        canContain.keys.fold("") {acc, bag -> "$acc\n${bag.colorCode}"}
}