use std::fs;
use std::env;

mod adapter;

fn main() {
    let args:Vec<String> = env::args().collect();
    read_input(args[1].as_str()).iter().for_each(|entry| println!("{}", entry));
}

fn read_input(path: &str) -> Vec<u32> {
    return fs::read_to_string(path)
    .expect("Failed to read the input file!")
    .split('\n')
    .map(|line| line.trim())
    .filter(|line| !line.is_empty())
    .map(|line| line.parse::<u32>().expect(format!("Failed to parse an entry: '{}'", line).as_str()))
    .collect();
}