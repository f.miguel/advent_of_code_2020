import sys
from functools import total_ordering
from tree import Node


@total_ordering
class Adapter:
    def __init__(self, joltage):
        self.joltage = joltage
        self.connection = None

    def can_connect(self, adapter):
        joltage_diff = self.joltage - adapter.joltage
        return 0 < joltage_diff < 4

    def __str__(self):
        return f'Adapter[joltage: {self.joltage}; connected: {self.connection is not None}]'

    def __eq__(self, other):
        if other is self:
            return True
        if other is None or not isinstance(other, Adapter):
            return False
        return self.joltage == other.joltage

    def __hash__(self):
        return self.joltage

    def __lt__(self, other):
        return self.joltage < other.joltage


def connect(connector, connected):
    if not connector.can_connect(connected):
        print(f'{connector} cannot connect to {connected}')
        sys.exit(1)
    connector.connection = connected


def connect_adapters(adapters):
    for i in range(1, len(adapters)):
        connect(adapters[i], adapters[i - 1])


def extract_diffs(adapters):
    lis = []
    for adapter in adapters:
        if adapter.connection is not None:
            lis.append(adapter.joltage - adapter.connection.joltage)
    return lis


def combinations_tree(adapters, node):
    return combinations_tree_aux(adapters, node, {})


# The cache is used for memoization of the children for every adapter
def combinations_tree_aux(adapters, node, cache):
    if len(adapters) == 1:
        return None

    if node.data in cache:
        node.children += cache[node.data]
    else:
        pivot = adapters[0]
        for adapter in adapters[1:]:
            if adapter.can_connect(pivot):
                node.children.append(Node(adapter))
            else: break
        cache[node.data] = node.children

        i = 1
        for child in node.children:
            combinations_tree_aux(adapters[i:], child, cache)
            i += 1

    return node


def count_leaves(node):
    """
    Returns the number of leaves in the given node
    """
    return __count_leaves_aux(node, {})


def __count_leaves_aux(node, cache):
    if len(node.children) == 0: # leaf
        return 1

    if node.data.joltage in cache:
        return cache[node.data.joltage]

    children_leaves_sum = 0
    for child in node.children:
        children_leaves_sum += __count_leaves_aux(child, cache)
    cache[node.data.joltage] = children_leaves_sum
    return children_leaves_sum
