#! /usr/bin/env node

fs = require('fs')
adapter = require('./adapter')
tree = require('./tree')


function readInput(path) {
    return fs.readFileSync(path, 'UTF-8')
             .split('\n')
             .map(line => line.trim())
             .filter(line => line.length !== 0)
             .map(entry => {
                const adapt = new adapter.Adapter(parseInt(entry))
                return adapt
                })
}


let adapters = readInput('../input.txt').sort(adapter.compareAdapters)

// add source
adapters = [new adapter.Adapter(0)].concat(adapters)

// add device
adapters = adapters.concat(new adapter.Adapter(adapters[adapters.length - 1].joltage + 3))

console.log(adapter.countConnDiffs(adapters))

const arrTree = adapter.arrangementsTree(adapters, new tree.Node(adapters[0]))

console.log(adapter.countLeaves(arrTree))