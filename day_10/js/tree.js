exports.Node = class {
    constructor(data) {
        this.data = data
        this.children = []
    }

    addChildren(children) {
        this.children = this.children.concat(children)
    }
}
